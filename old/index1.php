<!DOCTYPE html>
<html lang="id">

<head>
    <title>DGMind - Digital Mind</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  -->
    <meta name="description" content="Create live segments and target the right people for messages based on their behaviors." />
    <meta name="keywords" content="Create live segments and target the right people for messages based on their behaviors." />
    <meta property="og:title" content="DGMind - Digital Product Design Agency" />
    <meta property="og:description" content="Create live segments and target the right people for messages based on their behaviors." />
    <!-- <meta property="og:image" content="http://m-tani.com/images/logo_m-tani.png" />
    <meta property="og:url" content="https://www.m-tani.com/" />
    <meta property="og:type" content="website" /> -->

    <link rel="Shortcut icon" href="assets/images/logo.png">
    <link rel="stylesheet" href="assets/css/main1.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/splide.min.css?<?= time() ?>">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css?<?= time() ?>">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>

    <link rel="stylesheet" href="assets/css/humbleScroll.min.css?<?= time() ?>" />
    <script src="assets/js/humbleScroll.js?<?= time() ?>"></script>
</head>

<body>

    <?php include("component/navbar.php") ?>

    <?php
            if(isset($_GET['page'])){ 
                $url = 'index.php?';
                include("include/pages/".$_GET['page'].".php");
            }else{
                $url = 'index.php?';
                include("include/pages/home1.php");
            }
        ?>


        <?php  include("component/footer.php") ?>


        <script>
$(document).ready(function(){
  $('body').scrollspy({target: ".navbar", offset: 50});   
  $("#myNavbar a").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
        $('html, body').animate({
          scrollTop: $(hash).offset().top
          }, 800, function(){
        window.location.hash = hash;
      });
    }
  });
});
</script>


        <script>
            // Fire regular DSOS
            const scroll = new HumbleScroll({
                enableCallback: true,
                repeat: false,
                mirror: false,
                threshold: 0.10,
                offset: {
                    top: -64,
                    bottom: -150,
                },
            })

            // Fire custom DSOS with custom options
            const myCustomScroll = new HumbleScroll({
                element: '.my-custom-element',
                class: 'enter',
            })

            const verticalScroll = new HumbleScroll({
                root: document.querySelector('#horizontal-scroll'),
                element: '.my-custom-element',
                class: 'enter',
                repeat: true,
                mirror: true,
            })

            scroll.on('hs:complete', () => {
                console.log('HumbleScroll is complete!')
            })

            function firstCall() {
                const card = document.querySelector('#callback-1')
                const cardHeading = document.querySelector('#callback-1-heading')
                card.style = 'background-color: purple; border-radius: 100%; transform: scale(0.75);'
                setTimeout(() => {
                    cardHeading.innerHTML = 'Kinda cool right?'
                }, 4000)
            }

            function secondCall() {
                const section = document.querySelector('#callback-section')
                section.style = 'background-color: #27272a; color: #a3e635;'
            }
        </script>

</body>

</html>