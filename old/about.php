<div id="about-us">
    <div class="container">
        <div class="row">

            <div class="col-lg-6">
                <img src="assets/images/head-about-us.png" alt="img" class="about-us-1" />
                <img src="assets/images/head-about-us-2.png" alt="img" class="about-us-2" />
            </div>

            <div class="col-lg-6">
                <h5>About Us</h5>
                <h1>About my behavior to provide a solution</h1>
                <p>Create live segments and target the right people for messages based on their behaviors. Create live segments and target the right people for messages based on their behaviors.Create live segments and target the right people for messages
                    based on their behaviors.</p>
            </div>

        </div>
    </div>
</div>

<div id="visimisi">
    <div class="container">
        <div class="row">

            <div class="col-lg-6">
                <h5>Sejarah & Visi Misi</h5>
                <h1>About my behavior to provide a solution</h1>
                <p>Create live segments and target the right people for messages based on their behaviors. Create live segments and target the right people for messages based on their behaviors</p>
            </div>

            <div class="col-lg-6">
                <img src="assets/images/visimisi.png" alt="img" class="meta" />
            </div>

            <div class="col-lg-4">
                <div class="bg">
                    <h4>Sejarah</h4>
                    <img src="assets/images/sejarah.png" alt="img" class="sejarah" />
                    <span>Create live segments and target the right people for messages based on their behaviors. </span>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="bg">
                    <h4>Visi</h4>
                    <img src="assets/images/visi.png" alt="img" class="sejarah" />
                    <span>Create live segments and target the right people for messages based on their behaviors. </span>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="bg">
                    <h4>Misi</h4>
                    <img src="assets/images/misi.png" alt="img" class="sejarah" />
                    <span>Create live segments and target the right people for messages based on their behaviors. </span>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="testi">
    <div class="container">
        <h5>Our Team</h5>
        <h3>About my behavior to provide a solution</h3>
        <p>Create live segments and target the right people for messages based on their behaviors. Create live segments and target</p>
        <div class="row">
            <div class="col-lg-12">

                <section class="splide testi" aria-label="Splide Basic HTML Example">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img src="assets/images/foto.png" alt="img" class="foto" />
                                <h5>Our Team</h5>
                                <span>Create live segments and target</span>
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/foto.png" alt="img" class="foto" />
                                <h5>Our Team</h5>
                                <span>Create live segments and target</span>
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/foto.png" alt="img" class="foto" />
                                <h5>Our Team</h5>
                                <span>Create live segments and target</span>
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/foto.png" alt="img" class="foto" />
                                <h5>Our Team</h5>
                                <span>Create live segments and target</span>
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/foto.png" alt="img" class="foto" />
                                <h5>Our Team</h5>
                                <span>Create live segments and target</span>
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/foto.png" alt="img" class="foto" />
                                <h5>Our Team</h5>
                                <span>Create live segments and target</span>
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/foto.png" alt="img" class="foto" />
                                <h5>Our Team</h5>
                                <span>Create live segments and target</span>
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/foto.png" alt="img" class="foto" />
                                <h5>Our Team</h5>
                                <span>Create live segments and target</span>
                            </li>
                        </ul>
                    </div>
                </section>

            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
<script>
    var splide = new Splide('.splide.testi', {
        rewind: true,
        perPage: 4,
        autoplay: true,
        drag: false,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
        },
    });

    splide.mount();
</script>