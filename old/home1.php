<div id="home">
    <div class="container">
        <div class="row">

            <div class="col-xl-6">
                <h5>We have big mind</h5>
                <h1>Digital product design agency</h1>
                <p>Create live segments and target the right people for messages based on their behaviors.</p>
                <a href="#">
                    <div class="button">Get Started</div>
                </a>
            </div>

            <div class="bubbles">
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <!-- <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div> -->
            </div>

        </div>
    </div>
</div>

<div id="about">
    <div class="container">
        <div class="row">

            <div class="col-lg-7">
                <img src="assets/images/about1.png" alt="img" class="about1" />
                <img src="assets/images/about-bubble1.png" alt="img" class="bubble1" />
                <img src="assets/images/about-bubble2.png" alt="img" class="bubble2" />
            </div>

            <div class="col-lg-5">
                <h5>About Us</h5>
                <h3>About my behavior to provide a solution</h3>
                <p>In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus</p>
            </div>

        </div>
    </div>
</div>

<div id="service">
    <div class="container">
        <div class="row">

            <div class="col-lg-4">
                <h5>Service</h5>
                <h3>All the services about the design I have</h3>
                <p>Create live segments and target the right people for messages based on their behaviors.</p>
            </div>

            <div class="col-lg-8">
                <img src="assets/images/bg-service1.png" alt="img" class="bg-service" />
                <img src="assets/images/service1-1.png" alt="img" class="service-1" />
                <img src="assets/images/service1-2.png" alt="img" class="service-2" />
            </div>

        </div>

    </div>
</div>

<div id="layanan">
    <div class="container">
        <div class="row g-4">

            <div class="col-lg-4">
                <img src="assets/images/layanan-1.png" alt="img" class="layanan" />
                <h5>Design</h5>
                <p>Create live segments and target the right people for messages based on their</p>
            </div>

            <div class="col-lg-4">
                <img src="assets/images/layanan-2.png" alt="img" class="layanan" />
                <h5>Development</h5>
                <p>Create live segments and target the right people for messages based on their</p>
            </div>

            <div class="col-lg-4">
                <img src="assets/images/layanan-3.png" alt="img" class="layanan" />
                <h5>Online Marketing</h5>
                <p>Create live segments and target the right people for messages based on their</p>
            </div>

            <div class="col-lg-4">
                <img src="assets/images/layanan-1.png" alt="img" class="layanan" />
                <h5>Design</h5>
                <p>Create live segments and target the right people for messages based on their</p>
            </div>

            <div class="col-lg-4">
                <img src="assets/images/layanan-2.png" alt="img" class="layanan" />
                <h5>Development</h5>
                <p>Create live segments and target the right people for messages based on their</p>
            </div>

            <div class="col-lg-4">
                <img src="assets/images/layanan-3.png" alt="img" class="layanan" />
                <h5>Online Marketing</h5>
                <p>Create live segments and target the right people for messages based on their</p>
            </div>

        </div>
    </div>
</div>

<div id="porto">
    <div class="container">
        <h5>Portfolio</h5>
        <h3>Please see what I have done</h3>
        <span>In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus</span>
    </div>
    <ul class="nav nav-pills justify-content-center mb-3 mt-5" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-web" type="button" role="tab" aria-controls="pills-web" aria-selected="true">Web Design</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-print-tab" data-bs-toggle="pill" data-bs-target="#pills-print" type="button" role="tab" aria-controls="pills-print" aria-selected="false">Print Ad</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-Illustration-tab" data-bs-toggle="pill" data-bs-target="#pills-Illustration" type="button" role="tab" aria-controls="pills-Illustration" aria-selected="false">Illustration</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-video-tab" data-bs-toggle="pill" data-bs-target="#pills-video" type="button" role="tab" aria-controls="pills-video" aria-selected="false">Video Graphic</button>
        </li>

    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-web" role="tabpanel" aria-labelledby="pills-web-tab" tabindex="0">

            <section class="splide web" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>

            </section>

        </div>

        <div class="tab-pane fade" id="pills-print" role="tabpanel" aria-labelledby="pills-print-tab" tabindex="0">

            <section class="splide print" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>

            </section>

        </div>

        <div class="tab-pane fade" id="pills-Illustration" role="tabpanel" aria-labelledby="pills-Illustration-tab" tabindex="0">

            <section class="splide illustration" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>

            </section>

        </div>

        <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab" tabindex="0">

            <section class="splide video" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>

            </section>

        </div>
    </div>
</div>

<div id="client">
    <div class="container">
        <h5>Client</h5>
        <h3>Artwork we've done</h3>
        <span>In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus In vel varius turpis</span>
        <div class="row">
            <div class="col-lg-12">
                <section class="splide client" aria-label="Splide Basic HTML Example">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img src="assets/images/client.png" alt="img" />
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/client.png" alt="img" />
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/client.png" alt="img" />
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>



<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
<script>
    var splide = new Splide('.splide.web', {
        rewind: true,
        perPage: 2,
        autoplay: true,
        drag: false,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
        },
    });

    splide.mount();

    var splide = new Splide('.splide.print', {
        rewind: true,
        perPage: 2,
        autoplay: true,
        drag: false,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
        },
    });

    splide.mount();

    var splide = new Splide('.splide.illustration', {
        rewind: true,
        perPage: 2,
        autoplay: true,
        drag: false,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
        },
    });

    splide.mount();

    var splide = new Splide('.splide.video', {
        rewind: true,
        perPage: 2,
        autoplay: true,
        drag: false,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
        },
    });

    splide.mount();

    var splide = new Splide('.splide.client', {
        rewind: true,
        perPage: 1,
        autoplay: true,
        drag: true,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
        },
    });

    splide.mount();
</script>


<!-- <script>
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
</script> -->