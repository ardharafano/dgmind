<div id="footer">
    <div class="container">
        <div class="row g-4">

            <div class="col-lg-2 col-12">
                <h6>About Us</h6>
                <div class="menu">

                    <a href="#">
                        <div>Vision</div>
                    </a>

                    <a href="#">
                        <div>Mision</div>
                    </a>

                </div>
            </div>

            <div class="col-lg-3 col-12 services">
                <h6>Services</h6>
                <div class="menu">

                    <a href="?page=home#service">
                        <div>Website & App Development</div>
                    </a>


                    <a href="?page=home#service">
                        <div>Creative & Strategy</div>
                    </a>

                    <a href="?page=home#service">
                        <div>Video Production</div>
                    </a>

                    <a href="?page=home#service">
                        <div>Social Media Marketing</div>
                    </a>

                    <a href="?page=home#service">
                        <div>Event Organizer</div>
                    </a>

                    <a href="?page=home#service">
                        <div>Ad placement (iklandisini.com)</div>
                    </a>
                </div>
            </div>

            <div class="col-lg-4 col-12">
                <h6>Portfolio</h6>
                <div class="menu">

                    <a href="?page=home#porto">
                        <div>Web Design</div>
                    </a>

                    <a href="?page=home#porto">
                        <div>Print Ad</div>
                    </a>

                    <a href="?page=home#porto">
                        <div>Illustration</div>
                    </a>

                    <a href="?page=home#porto">
                        <div>Video Graphic</div>
                    </a>

                </div>
            </div>

            <div class="col-lg-3 col-12 dgmind">
                <h6>DGMind.id</h6>
                <p>Jl. Mega Kuningan Timur Blok C6 Kav.9 Kawasan Mega Kuningan, Jakarta 12950, Indonesia
                </p>
                <p>Telepon : 021 - 50101239 <br>Email : hello@dgmind.id</p>
                <div class="sosmed">
                    <a href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/ig.svg" alt="image" width="30" height="auto" /></a>
                    <a href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/twt.svg" alt="image" width="30" height="auto" /></a>
                    <a href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/vt.svg" alt="image" width="30" height="auto" /></a>
                    <a href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/fb.svg" alt="image" width="30" height="auto" /></a>
                </div>
            </div>

        </div>
    </div>
</div>



<div id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h6>© Copyright 2022 DGMind - All Rights Reserved.</h6>
            </div>
        </div>
    </div>
</div>