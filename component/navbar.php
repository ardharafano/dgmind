<div id="nav">
    <nav class="navbar navbar-expand-xl navbar-light bg-light fixed-top" aria-label="Ninth navbar example" id="nav-lokal" style="transition: 0.5s;">
        <div class="container">
            <a class="navbar-brand" href="index.php"><img src="assets/images/logo.svg" alt="image" id="logo-ikn" /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

            <div class="nav-dg">
                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="#about">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#service">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#porto">Portfolio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#kontak">Contact</a>
                        </li>
                    </ul>

                    <div class="desktop">
                        <ul class="nav-dg-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/ig.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/twt.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/vt.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/fb.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                        </ul>
                    </div>

                    <div class="mobile">
                        <ul class="nav-dg-right">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/ig.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/twt.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/vt.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/fb.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </nav>
</div>