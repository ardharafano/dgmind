<div id="nav">
    <nav class="navbar navbar-expand-xl navbar-light bg-light fixed-top" aria-label="Ninth navbar example" id="nav-lokal" style="transition: 0.5s;">
        <div class="container">
            <a class="navbar-brand" href="index.php"><img src="assets/images/logo.svg" alt="image" id="logo-ikn" /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

            <div class="nav-dg">
                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="?page=home#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="?page=home#about">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="?page=home#service">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="?page=home#porto">Portfolio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="?page=home#kontak">Contact</a>
                        </li>
                    </ul>

                    <div class="desktop">
                        <ul class="nav-dg-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/ig.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/twt.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/vt.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/fb.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                        </ul>
                    </div>

                    <div class="mobile">
                        <ul class="nav-dg-right">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/ig.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/twt.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/vt.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/sosmed/fb.svg" alt="image" width="30" height="auto" /></a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </nav>
</div>

<div id="porto">
    <div class="container">
        <h5>Portfolio</h5>
        <h3>Please see what I have done</h3>
        <span>In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus</span>
    </div>
    <ul class="nav nav-pills justify-content-center mb-3 mt-5" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-web" type="button" role="tab" aria-controls="pills-web" aria-selected="true">Web Design</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-print-tab" data-bs-toggle="pill" data-bs-target="#pills-print" type="button" role="tab" aria-controls="pills-print" aria-selected="false">Print Ad</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-Illustration-tab" data-bs-toggle="pill" data-bs-target="#pills-Illustration" type="button" role="tab" aria-controls="pills-Illustration" aria-selected="false">Illustration</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-video-tab" data-bs-toggle="pill" data-bs-target="#pills-video" type="button" role="tab" aria-controls="pills-video" aria-selected="false">Video Graphic</button>
        </li>

    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-web" role="tabpanel" aria-labelledby="pills-web-tab" tabindex="0">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="pagination">
                        <a href="#">&laquo;</a>
                        <a href="#">1</a>
                        <a href="#" class="active">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#">&raquo;</a>
                    </div>


                </div>
            </div>

        </div>

        <div class="tab-pane fade" id="pills-print" role="tabpanel" aria-labelledby="pills-print-tab" tabindex="0">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="pagination">
                        <a href="#">&laquo;</a>
                        <a href="#">1</a>
                        <a href="#" class="active">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#">&raquo;</a>
                    </div>

                </div>
            </div>

        </div>

        <div class="tab-pane fade" id="pills-Illustration" role="tabpanel" aria-labelledby="pills-Illustration-tab" tabindex="0">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="pagination">
                        <a href="#">&laquo;</a>
                        <a href="#">1</a>
                        <a href="#" class="active">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#">&raquo;</a>
                    </div>

                </div>
            </div>

        </div>

        <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab" tabindex="0">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="pagination">
                        <a href="#">&laquo;</a>
                        <a href="#">1</a>
                        <a href="#" class="active">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#">&raquo;</a>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>