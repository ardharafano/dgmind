<div id="home">
    <div class="container">
        <div class="row">

            <div class="col-xl-6">
                <h5>We have big mind</h5>
                <h1>Digital product design agency</h1>
                <p>Create live segments and target the right people for messages based on their behaviors.</p>
                <a href="#">
                    <div class="button">Get Started</div>
                </a>
            </div>

            <div class="col-xl-6">
                <!-- <img src="assets/images/header.png" alt="img" class="header" /> -->
                <img src="assets/images/people.png" alt="img" class="people" />
                <img src="assets/images/astro2.png" alt="img" class="astro" />
                <img src="assets/images/lamp.png" alt="img" class="lamp" />
                <img src="assets/images/bubble.png" alt="img" class="bubble" />
            </div>

        </div>
    </div>
</div>

<div id="about">
    <div class="container">
        <div class="row">

            <div class="col-lg-8">
                <img src="assets/images/about.png" alt="img" class="about" />
                <!-- <img src="assets/images/menara.png" alt="img" class="menara" />
                <img src="assets/images/awan-1.png" alt="img" class="awan-1" />
                <img src="assets/images/awan-2.png" alt="img" class="awan-2" />
                <img src="assets/images/awan-3.png" alt="img" class="awan-3" /> -->
            </div>

            <div class="col-lg-4">
                <h5>About Us</h5>
                <h3>DG Mind Creative Solution</h3>
                <p>No matter what your business needs, we can connect you with a creative No matter what your business needs, we can connect you with a creative</p>

                <h5>Vision</h5>
                <p>Because good design makes great business, Aenean in efficitur ipsum, inegestas ipsum</p>

                <h5>Mision</h5>
                <p>We can connect you with a creative expert to make your business look and feel</p>
            </div>

        </div>
    </div>
</div>

<div id="service">
    <div class="container">
        <div class="row">

            <div class="col-lg-4">
                <h5>Service</h5>
                <h3>All the services about the design I have</h3>
                <p>Create live segments and target the right people for messages based on their behaviors.</p>
            </div>

            <div class="col-lg-8">
                <img src="assets/images/bg-service.png" alt="img" class="bg-service" />
                <img src="assets/images/service-1.png" alt="img" class="service-1" />
                <img src="assets/images/service-2.png" alt="img" class="service-2" />
            </div>

        </div>

    </div>
</div>

<div id="layanan">
    <div class="container">
        <div class="row g-4">

            <div class="col-lg-4">
                <element type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    <img src="assets/images/layanan-2.png" alt="img" class="layanan" />
                    <h5>Website & App Development</h5>
                    <p>Create live segments and target the right people for messages based on their</p>
                </element>

                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><b>Website & App Development</b></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div> -->
                            <div class="modal-body">
                                <div class="container">
                                    <img src="assets/images/layanan-2.png" alt="img" class="layanan-modal" />
                                    <p>Create live segments and target the right people for messages based on their</p>
                                    <div class="clear"></div>
                                    <b>Website & App Development</b>
                                    <p>Visual Design, Web Development, Mobile Apps, Ecommerce</p>
                                    <div class="clear"></div>
                                    <b>Hosting & Security</b>
                                    <p>Hosting, Web Security, Maintenance, Email Hosting</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn" data-bs-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <element type="button" data-bs-toggle="modal" data-bs-target="#exampleModal2">
                    <img src="assets/images/layanan-1.png" alt="img" class="layanan" />
                    <h5>Creative & Strategy</h5>
                    <p>Create live segments and target the right people for messages based on their</p>
                </element>

                <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel2"><b>Creative & Strategy</b></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div> -->
                            <div class="modal-body">
                                <div class="container">
                                    <img src="assets/images/layanan-1.png" alt="img" class="layanan-modal" />
                                    <p>Create live segments and target the right people for messages based on their</p>
                                    <div class="clear"></div>
                                    <b>Creative & Strategy</b>
                                    <p>Logo Design, Company Profile, Annual Report Design, Brochure Design, Packaging Design, Infographic Design, Banner Design, Brand Guideline Design</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <element type="button" data-bs-toggle="modal" data-bs-target="#exampleModal3">
                    <img src="assets/images/layanan-4.png" alt="img" class="layanan" />
                    <h5>Video Production</h5>
                    <p>Create live segments and target the right people for messages based on their</p>
                </element>

                <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel3" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel3"><b>Video Production</b></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div> -->
                            <div class="modal-body">
                                <div class="container">
                                    <img src="assets/images/layanan-4.png" alt="img" class="layanan-modal" />
                                    <p>Create live segments and target the right people for messages based on their</p>
                                    <div class="clear"></div>
                                    <b>Video Production</b>
                                    <p>Commercial Video / TVC, Digital Video Production, Web Series Production, Testimonial Video, Company Profile Video, Documentation Video, Live Streaming (Webinar), Infographic / 2D animation</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <element type="button" data-bs-toggle="modal" data-bs-target="#exampleModal4">
                    <img src="assets/images/layanan-3.png" alt="img" class="layanan" />
                    <h5>Social Media Marketing</h5>
                    <p>Create live segments and target the right people for messages based on their</p>
                </element>

                <div class="modal fade" id="exampleModal4" tabindex="-1" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel4"><b>Social Media Marketing</b></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div> -->
                            <div class="modal-body">
                                <div class="container">
                                    <img src="assets/images/layanan-3.png" alt="img" class="layanan-modal" />
                                    <p>Create live segments and target the right people for messages based on their</p>
                                    <div class="clear"></div>
                                    <b>Social Media Marketing</b>
                                    <p>Social Media Management, Community Management, KOL Management</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <element type="button" data-bs-toggle="modal" data-bs-target="#exampleModal5">
                    <img src="assets/images/layanan-5.png" alt="img" class="layanan" />
                    <h5>Event Organizer</h5>
                    <p>Create live segments and target the right people for messages based on their</p>
                </element>

                <div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLabel5" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel5"><b>Event Organizer</b></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div> -->
                            <div class="modal-body">
                                <div class="container">
                                    <img src="assets/images/layanan-5.png" alt="img" class="layanan-modal" />
                                    <p>Create live segments and target the right people for messages based on their</p>
                                    <div class="clear"></div>
                                    <b>Event Organizer</b>
                                    <p>Seminar, Workshop, Gathering, Team Building, Outbond, Training, Custom Activation</p>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <element type="button" data-bs-toggle="modal" data-bs-target="#exampleModal6">
                    <img src="assets/images/layanan-6.png" alt="img" class="layanan" />
                    <h5>Ad placement (iklandisini.com)</h5>
                    <p>Create live segments and target the right people for messages based on their</p>
                </element>

                <div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel6" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel6"><b>Ad placement (iklandisini.com)</b></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div> -->
                            <div class="modal-body">
                                <div class="container">
                                    <img src="assets/images/layanan-6.png" alt="img" class="layanan-modal" />
                                    <p>Create live segments and target the right people for messages based on their</p>
                                    <div class="clear"></div>
                                    <b>Ad placement (iklandisini.com)</b>
                                    <p>Display Banner, Advertorial, Sosmed, Video, Display TV</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<!-- <div id="porto">
    <div class="container">
        <h5>Portfolio</h5>
        <h3>Please see what I have done</h3>
        <span>In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus</span>
    </div>
    <ul class="nav nav-pills justify-content-center mb-3 mt-5" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-web" type="button" role="tab" aria-controls="pills-web" aria-selected="true">Web Design</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-print-tab" data-bs-toggle="pill" data-bs-target="#pills-print" type="button" role="tab" aria-controls="pills-print" aria-selected="false">Print Ad</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-Illustration-tab" data-bs-toggle="pill" data-bs-target="#pills-Illustration" type="button" role="tab" aria-controls="pills-Illustration" aria-selected="false">Illustration</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-video-tab" data-bs-toggle="pill" data-bs-target="#pills-video" type="button" role="tab" aria-controls="pills-video" aria-selected="false">Video Graphic</button>
        </li>

    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-web" role="tabpanel" aria-labelledby="pills-web-tab" tabindex="0">

            <section class="splide web" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>

            </section>

        </div>

        <div class="tab-pane fade" id="pills-print" role="tabpanel" aria-labelledby="pills-print-tab" tabindex="0">

            <section class="splide print" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>

            </section>

        </div>

        <div class="tab-pane fade" id="pills-Illustration" role="tabpanel" aria-labelledby="pills-Illustration-tab" tabindex="0">

            <section class="splide illustration" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>

            </section>

        </div>

        <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab" tabindex="0">

            <section class="splide video" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>

                                    <div class="col-lg-12">
                                        <img src="assets/images/porto.png" alt="img" />
                                        <h4>Microsite IKN</h4>
                                        <p>Create live segments and target the right people for messages based on their</p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>

            </section>

        </div>
    </div>
</div> -->

<div id="porto">
    <div class="container">
        <h5>Portfolio</h5>
        <h3>Please see what I have done</h3>
        <span>In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus</span>
    </div>
    <ul class="nav nav-pills justify-content-center mb-3 mt-5" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-web" type="button" role="tab" aria-controls="pills-web" aria-selected="true">Web Design</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-print-tab" data-bs-toggle="pill" data-bs-target="#pills-print" type="button" role="tab" aria-controls="pills-print" aria-selected="false">Print Ad</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-Illustration-tab" data-bs-toggle="pill" data-bs-target="#pills-Illustration" type="button" role="tab" aria-controls="pills-Illustration" aria-selected="false">Illustration</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-video-tab" data-bs-toggle="pill" data-bs-target="#pills-video" type="button" role="tab" aria-controls="pills-video" aria-selected="false">Video Graphic</button>
        </li>

    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-web" role="tabpanel" aria-labelledby="pills-web-tab" tabindex="0">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <div class="tab-pane fade" id="pills-print" role="tabpanel" aria-labelledby="pills-print-tab" tabindex="0">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <div class="tab-pane fade" id="pills-Illustration" role="tabpanel" aria-labelledby="pills-Illustration-tab" tabindex="0">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab" tabindex="0">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="#">
                            <img src="assets/images/porto.png" alt="img" />
                            <h4>Microsite IKN</h4>
                            <p>Create live segments and target the right people for messages based on their</p>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <a href="?page=detail">
        <div type="button" class="detail">LEBIH DETAIL</div>
    </a>

</div>

<div id="client">
    <div class="container">
        <h5>Client</h5>
        <h3>Artwork we've done</h3>
        <span>In vel varius turpis, non dictum sem. Aenean in efficitur ipsum, inegestas ipsum. Mauris in mi ac tellus In vel varius turpis</span>
        <div class="row">
            <div class="col-lg-12">
                <section class="splide client" aria-label="Splide Basic HTML Example">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <div class="bg">
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                </div>
                            </li>
                            <li class="splide__slide">
                                <div class="bg">
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                </div>
                            </li>
                            <li class="splide__slide">
                                <div class="bg">
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                    <img src="assets/images/clickmov.png" alt="img" />
                                    <img src="assets/images/beritahits.png" alt="img" />
                                    <img src="assets/images/yoursay.png" alt="img" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<div id="kontak">
    <div class="container">
        <div class="row">
            <h5>Contact</h5>
            <form class="d-flex justify-content-center row g-3">
                <div class="col-lg-6">
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama">
                    </div>
                    <div class="form-group mb-3">
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
                    </div>
                    <div class="form-group mb-3">
                        <input type="number" class="form-control" id="exampleInputPassword1" placeholder="No. Handphone">
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" placeholder="Pesan"></textarea>
                    </div>
                </div>

                <button type="submit" class="btn"><img src="assets/images/kirim.png" alt="img" /> Kirim</button>

            </form>

        </div>
    </div>
</div>



<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
<script>
    // var splide = new Splide('.splide.web', {
    //     rewind: true,
    //     perPage: 2,
    //     autoplay: true,
    //     drag: false,
    //     breakpoints: {
    //         991.98: {
    //             perPage: 1,
    //             gap: "1rem",
    //             drag: true,
    //         },
    //         768: {
    //             perPage: 1,
    //             gap: "1rem",
    //             drag: true,
    //         },
    //     },
    // });

    // splide.mount();

    // var splide = new Splide('.splide.print', {
    //     rewind: true,
    //     perPage: 2,
    //     autoplay: true,
    //     drag: false,
    //     breakpoints: {
    //         991.98: {
    //             perPage: 1,
    //             gap: "1rem",
    //             drag: true,
    //         },
    //         768: {
    //             perPage: 1,
    //             gap: "1rem",
    //             drag: true,
    //         },
    //     },
    // });

    // splide.mount();

    // var splide = new Splide('.splide.illustration', {
    //     rewind: true,
    //     perPage: 2,
    //     autoplay: true,
    //     drag: false,
    //     breakpoints: {
    //         991.98: {
    //             perPage: 1,
    //             gap: "1rem",
    //             drag: true,
    //         },
    //         768: {
    //             perPage: 1,
    //             gap: "1rem",
    //             drag: true,
    //         },
    //     },
    // });

    // splide.mount();

    // var splide = new Splide('.splide.video', {
    //     rewind: true,
    //     perPage: 2,
    //     autoplay: true,
    //     drag: false,
    //     breakpoints: {
    //         991.98: {
    //             perPage: 1,
    //             gap: "1rem",
    //             drag: true,
    //         },
    //         768: {
    //             perPage: 1,
    //             gap: "1rem",
    //             drag: true,
    //         },
    //     },
    // });

    // splide.mount();

    var splide = new Splide('.splide.client', {
        rewind: true,
        perPage: 1,
        autoplay: true,
        drag: true,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                drag: true,
            },
        },
    });

    splide.mount();
</script>